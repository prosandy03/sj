
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>ARBNB E Commarce</title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css" />

	<!-- Slick -->
	<link type="text/css" rel="stylesheet" href="assets/css/slick.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/slick-theme.css" />

	<!-- nouislider -->
	<link type="text/css" rel="stylesheet" href="assets/css/nouislider.min.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="assets/css/style.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

</head>

<body>
	<!-- HEADER -->
	<header>
		<!-- top Header -->
		
		<!-- /top Header -->

		<!-- header -->
		<div id="header">
			<div class="container">
				<div class="pull-left">
					<!-- Logo -->
					<div class="header-logo">
						<a class="logo" href="#">
							<img src="./assets/img/logo.png" alt="">
						</a>
					</div>
					<!-- /Logo -->
                    <li class="nav-toggle">
							<button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
						</li>
					<!-- Search -->
					<div class="header-search">
						<form action="search?" method="get">
							<input class="input search-input" type="text" placeholder="Enter your keyword">
							<select class="input search-categories">
								<option value="0">All India</option>
								<option value="1">Raipur</option>
                                <option value="2">Bhilai</option>
                                <option value="3">Durg</option>
								
							</select>
							<button class="search-btn"><i class="fa fa-search"></i></button>
						</form>
					</div>
					<!-- /Search -->
				</div>
				<div class="pull-right">
					<ul class="header-btns">
						<!-- Account -->
						<li class="header-account dropdown default-dropdown">
							<div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
								<div class="header-btns-icon">
                                <i class="fas fa-user"></i>
								</div>
								<strong class="text-uppercase">My Account <i class="fa fa-caret-down"></i></strong>
							</div>
							<a href="#" class="text-uppercase" data-toggle="modal" data-target="#modalQuickView">Login</a> / <a href="#" class="text-uppercase" data-toggle="modal" data-target="#modalQuickView2">Join</a>
							<ul class="custom-menu">
								<li class="pointesrcurs"><a href="orders"><i class="fas fa-box"></i> My Order</a></li>
								<li class="pointesrcurs"><a href="#"><i class="fas fa-heart"></i> My Wishlist</a></li>
								<li class="pointesrcurs"><a href="#"><i class="fas fa-sign-out-alt"></i> Log out</a></li>
							</ul>
						</li>
						<!-- /Account -->

						<!-- Cart -->
						<li class="header-cart dropdown default-dropdown" >
							<a href="checkout">
								<div class="header-btns-icon">
									<i class="fa fa-shopping-cart"></i>
									<span class="qty">2</span>
								</div>
								<strong class="text-uppercase">My Cart:</strong>
								<br>
								<span>35.20₹</span>
							</a>
							
						</li>
						<!-- /Cart -->

						<!-- Mobile nav toggle-->
						
						<!-- / Mobile nav toggle -->
					</ul>
				</div>
			</div>
			<!-- header -->
		</div>
		<!-- container -->
	</header>
	<!-- /HEADER -->

	<!-- NAVIGATION -->
	<div id="navigation">
		<!-- container -->
		<div class="container">
			<div id="responsive-nav">
				<!-- category nav -->
				<div class="category-nav">
					<span class="category-header">Categories <i class="fa fa-list"></i></span>
					<ul class="category-list">
						<li class="dropdown side-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Women’s Clothing <i class="fa fa-angle-right"></i></a>
							<div class="custom-menu">
								<div class="row">
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Categories</h3></li>
											<li><a href="#">Women’s Clothing</a></li>
											<li><a href="#">Men’s Clothing</a></li>
											<li><a href="#">Phones & Accessories</a></li>
											<li><a href="#">Jewelry & Watches</a></li>
											<li><a href="#">Bags & Shoes</a></li>
										</ul>
										<hr class="hidden-md hidden-lg">
									</div>
									<!--<div class="col-md-4">-->
									<!--	<ul class="list-links">-->
									<!--		<li>-->
									<!--			<h3 class="list-links-title">Categories</h3></li>-->
									<!--		<li><a href="#">Women’s Clothing</a></li>-->
									<!--		<li><a href="#">Men’s Clothing</a></li>-->
									<!--		<li><a href="#">Phones & Accessories</a></li>-->
									<!--		<li><a href="#">Jewelry & Watches</a></li>-->
									<!--		<li><a href="#">Bags & Shoes</a></li>-->
									<!--	</ul>-->
									<!--	<hr class="hidden-md hidden-lg">-->
									<!--</div>-->
							</div>
						</li>
						<li><a href="#">Men’s Clothing</a></li>
						<li class="dropdown side-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Phones & Accessories <i class="fa fa-angle-right"></i></a>
							<div class="custom-menu">
								<div class="row">
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Categories</h3></li>
											<li><a href="#">Women’s Clothing</a></li>
											<li><a href="#">Men’s Clothing</a></li>
											<li><a href="#">Phones & Accessories</a></li>
											<li><a href="#">Jewelry & Watches</a></li>
											<li><a href="#">Bags & Shoes</a></li>
										</ul>
										<!--<hr>-->
										<!--<ul class="list-links">-->
										<!--	<li>-->
										<!--		<h3 class="list-links-title">Categories</h3></li>-->
										<!--	<li><a href="#">Women’s Clothing</a></li>-->
										<!--	<li><a href="#">Men’s Clothing</a></li>-->
										<!--	<li><a href="#">Phones & Accessories</a></li>-->
										<!--	<li><a href="#">Jewelry & Watches</a></li>-->
										<!--	<li><a href="#">Bags & Shoes</a></li>-->
										<!--</ul>-->
										<!--<hr class="hidden-md hidden-lg">-->
									</div>
									<!--<div class="col-md-4">-->
									<!--	<ul class="list-links">-->
									<!--		<li>-->
									<!--			<h3 class="list-links-title">Categories</h3></li>-->
									<!--		<li><a href="#">Women’s Clothing</a></li>-->
									<!--		<li><a href="#">Men’s Clothing</a></li>-->
									<!--		<li><a href="#">Phones & Accessories</a></li>-->
									<!--		<li><a href="#">Jewelry & Watches</a></li>-->
									<!--		<li><a href="#">Bags & Shoes</a></li>-->
									<!--	</ul>-->
									<!--	<hr>-->
									<!--	<ul class="list-links">-->
									<!--		<li>-->
									<!--			<h3 class="list-links-title">Categories</h3></li>-->
									<!--		<li><a href="#">Women’s Clothing</a></li>-->
									<!--		<li><a href="#">Men’s Clothing</a></li>-->
									<!--		<li><a href="#">Phones & Accessories</a></li>-->
									<!--		<li><a href="#">Jewelry & Watches</a></li>-->
									<!--		<li><a href="#">Bags & Shoes</a></li>-->
									<!--	</ul>-->
									<!--</div>-->
									<!--<div class="col-md-4 hidden-sm hidden-xs">-->
									<!--	<a class="banner banner-2" href="#">-->
									<!--		<img src="./assets/img/banner04.jpg" alt="">-->
									<!--		<div class="banner-caption">-->
									<!--			<h3 class="white-color">NEW<br>COLLECTION</h3>-->
									<!--		</div>-->
									<!--	</a>-->
									<!--</div>-->
								</div>
							</div>
						</li>
						<li><a href="#">Computer & Office</a></li>
						<li><a href="#">Consumer Electronics</a></li>
						<li class="dropdown side-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Jewelry & Watches <i class="fa fa-angle-right"></i></a>
							<div class="custom-menu">
								<div class="row">
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Categories</h3></li>
											<li><a href="#">Women’s Clothing</a></li>
											<li><a href="#">Men’s Clothing</a></li>
											<li><a href="#">Phones & Accessories</a></li>
											<li><a href="#">Jewelry & Watches</a></li>
											<li><a href="#">Bags & Shoes</a></li>
										</ul>
										<!--<hr>-->
										<!--<ul class="list-links">-->
										<!--	<li>-->
										<!--		<h3 class="list-links-title">Categories</h3></li>-->
										<!--	<li><a href="#">Women’s Clothing</a></li>-->
										<!--	<li><a href="#">Men’s Clothing</a></li>-->
										<!--	<li><a href="#">Phones & Accessories</a></li>-->
										<!--	<li><a href="#">Jewelry & Watches</a></li>-->
										<!--	<li><a href="#">Bags & Shoes</a></li>-->
										<!--</ul>-->
										<!--<hr class="hidden-md hidden-lg">-->
									</div>
									<!--<div class="col-md-4">-->
									<!--	<ul class="list-links">-->
									<!--		<li>-->
									<!--			<h3 class="list-links-title">Categories</h3></li>-->
									<!--		<li><a href="#">Women’s Clothing</a></li>-->
									<!--		<li><a href="#">Men’s Clothing</a></li>-->
									<!--		<li><a href="#">Phones & Accessories</a></li>-->
									<!--		<li><a href="#">Jewelry & Watches</a></li>-->
									<!--		<li><a href="#">Bags & Shoes</a></li>-->
									<!--	</ul>-->
									<!--	<hr>-->
									<!--	<ul class="list-links">-->
									<!--		<li>-->
									<!--			<h3 class="list-links-title">Categories</h3></li>-->
									<!--		<li><a href="#">Women’s Clothing</a></li>-->
									<!--		<li><a href="#">Men’s Clothing</a></li>-->
									<!--		<li><a href="#">Phones & Accessories</a></li>-->
									<!--		<li><a href="#">Jewelry & Watches</a></li>-->
									<!--		<li><a href="#">Bags & Shoes</a></li>-->
									<!--	</ul>-->
									<!--	<hr class="hidden-md hidden-lg">-->
									<!--</div>-->
									<!--<div class="col-md-4">-->
									<!--	<ul class="list-links">-->
									<!--		<li>-->
									<!--			<h3 class="list-links-title">Categories</h3></li>-->
									<!--		<li><a href="#">Women’s Clothing</a></li>-->
									<!--		<li><a href="#">Men’s Clothing</a></li>-->
									<!--		<li><a href="#">Phones & Accessories</a></li>-->
									<!--		<li><a href="#">Jewelry & Watches</a></li>-->
									<!--		<li><a href="#">Bags & Shoes</a></li>-->
									<!--	</ul>-->
									<!--	<hr>-->
									<!--	<ul class="list-links">-->
									<!--		<li>-->
									<!--			<h3 class="list-links-title">Categories</h3></li>-->
									<!--		<li><a href="#">Women’s Clothing</a></li>-->
									<!--		<li><a href="#">Men’s Clothing</a></li>-->
									<!--		<li><a href="#">Phones & Accessories</a></li>-->
									<!--		<li><a href="#">Jewelry & Watches</a></li>-->
									<!--		<li><a href="#">Bags & Shoes</a></li>-->
									<!--	</ul>-->
									<!--</div>-->
								</div>
							</div>
						</li>
						<li><a href="#">Bags & Shoes</a></li>
						<li><a href="#">View All</a></li>
					</ul>
				</div>
				<!-- /category nav -->

				<!-- menu nav -->
				<div class="menu-nav">
					<span class="menu-header">Menu <i class="fa fa-bars"></i></span>
					<ul class="menu-list">
						
                        <li><a href="#">Online Products</a></li>
                        
						<li><a href="#">Sales</a></li>
						<li class="dropdown default-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Special Offer <i class="fa fa-caret-down"></i></a>
							<ul class="custom-menu">
								<li><a href="#">Summer Sale</a></li>
								<li><a href="#">70% off Sale</a></li>
								<li><a href="#">groccersy Sale</a></li>
								
							</ul>
						</li>
					</ul>
				</div>
				<!-- menu nav -->
			</div>
		</div>
		<!-- /container -->
	</div>
	<!-- /NAVIGATION -->

	<!-- HOME -->
	<div id="home">
		<!-- container -->
		<div class="container">
			<!-- home wrap -->
			<div class="home-wrap">
				<!-- home slick -->
				<div id="home-slick">
					<!-- banner -->
					<div class="banner banner-1">
						<img src="./assets/img/banner01.jpg" alt="">
						<div class="banner-caption text-center">
							<h1>Bags sale</h1>
							<h3 class="white-color font-weak">Up to 50% Discount</h3>
							<button class="primary-btn">Shop Now</button>
						</div>
					</div>
					<!-- /banner -->

					<!-- banner -->
					<div class="banner banner-1">
						<img src="./assets/img/banner02.jpg" alt="">
						<div class="banner-caption">
							<h1 class="primary-color">HOT DEAL<br><span class="white-color font-weak">Up to 50% OFF</span></h1>
							<button class="primary-btn">Shop Now</button>
						</div>
					</div>
					<!-- /banner -->

					<!-- banner -->
					<div class="banner banner-1">
						<img src="./assets/img/banner03.jpg" alt="">
						<div class="banner-caption">
							<h1 class="white-color">New Product <span>Collection</span></h1>
							<button class="primary-btn">Shop Now</button>
						</div>
					</div>
					<!-- /banner -->
				</div>
				<!-- /home slick -->
			</div>
			<!-- /home wrap -->
		</div>
		<!-- /container -->
	</div>
	<!-- /HOME -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- banner -->
				<div class="col-md-4 col-xs-4 ">
					<a class="banner banner-1" href="#">
						<img src="./assets/img/banner10.jpg" alt="">
						<div class="banner-caption text-center">
							<h2 class="white-color shrtbanh1">NEW COLLECTION</h2>
						</div>
					</a>
				</div>
				<!-- /banner -->

				<!-- banner -->
				<div class="col-md-4 col-xs-4 ">
					<a class="banner banner-1" href="#">
						<img src="./assets/img/banner11.jpg" alt="">
						<div class="banner-caption text-center">
							<h2 class="white-color shrtbanh1">NEW COLLECTION</h2>
						</div>
					</a>
				</div>
				<!-- /banner -->

				<!-- banner -->
				<div class="col-md-4  col-xs-4">
					<a class="banner banner-1" href="#">
						<img src="./assets/img/banner12.jpg" alt="">
						<div class="banner-caption text-center">
							<h2 class="white-color shrtbanh1">NEW COLLECTION</h2>
						</div>
					</a>
				</div>
				<!-- /banner -->

			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- section-title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">New Arrival</h2>
						<div class="pull-right">
							<div class="product-slick-dots-1 custom-dots"></div>
						</div>
					</div>
				</div>
				<!-- /section-title -->

				<!-- banner -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="banner banner-2">
						<img src="./assets/img/banner14.jpg" alt="">
						<div class="banner-caption">
							<h2 class="white-color">NEW<br>COLLECTION</h2>
							<button class="primary-btn">Shop Now</button>
						</div>
					</div>
				</div>
				<!-- /banner -->

				<!-- Product Slick -->
				<div class="col-md-9 col-sm-6 col-xs-6">
					<div class="row">
						<div id="product-slick-1" class="product-slick">
							<!-- Product Single -->
							<div class="product product-single">
								<div class="product-thumb">
									<div class="product-label">
										<span>New</span> 
										
									</div>
									
									
									<img src="./assets/img/product01.jpg" alt="">
								</div>
								<div class="product-body">
									<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
									
									<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
									<div class="product-btns">
										<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
										
										<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
									</div>
								</div>
							</div>
							<!-- /Product Single -->

							<!-- Product Single -->
							<div class="product product-single">
								<div class="product-thumb">
									<div class="product-label">
										
									</div>
									
									
									<img src="./assets/img/product07.jpg" alt="">
								</div>
								<div class="product-body">
									<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
									
									<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
									<div class="product-btns">
										<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
										
										<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
									</div>
								</div>
							</div>
							<!-- /Product Single -->

							<!-- Product Single -->
							<div class="product product-single">
								<div class="product-thumb">
									<div class="product-label">
										<span>New</span>
										
									</div>
									
									
									<img src="./assets/img/product06.jpg" alt="">
								</div>
								<div class="product-body">
									<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
									
									<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
									<div class="product-btns">
										<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
										
										<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
									</div>
								</div>
							</div>
							<!-- /Product Single -->

							<!-- Product Single -->
							<div class="product product-single">
								<div class="product-thumb">
									<div class="product-label">
										<span>New</span>
										
									</div>
									
									
									<img src="./assets/img/product08.jpg" alt="">
								</div>
								<div class="product-body">
									<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
									
									<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
									<div class="product-btns">
										<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
										
										<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
									</div>
								</div>
							</div>
							<!-- /Product Single -->
						</div>
					</div>
				</div>
				<!-- /Product Slick -->
			</div>
			<!-- /row -->

			<!-- row -->
			<div class="row">
				<!-- section title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Deals Of The Day</h2>
						<div class="pull-right">
							<div class="product-slick-dots-2 custom-dots">
							</div>
						</div>
					</div>
				</div>
				<!-- section title -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single product-hot">
						<div class="product-thumb">
							<div class="product-label">
								
							</div>
							
							<button class="main-btn quick-view"><i class="fa fa-heart"></i></button>
							<img src="./assets/img/product01.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->

				<!-- Product Slick -->
				<div class="col-md-9 col-sm-6 col-xs-6">
					<div class="row">
						<div id="product-slick-2" class="product-slick">
							<!-- Product Single -->
							<div class="product product-single">
								<div class="product-thumb">
									<button class="main-btn quick-view"><i class="fa fa-heart"></i></button>
									<img src="./assets/img/product06.jpg" alt="">
								</div>
								<div class="product-body">
									<h3 class="product-price">₹32.50</h3>
									
									<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
									<div class="product-btns">
										<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
										
										<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
									</div>
								</div>
							</div>
							<!-- /Product Single -->

							<!-- Product Single -->
							<div class="product product-single">
								<div class="product-thumb">
									<div class="product-label">
										<button class="main-btn quick-view"><i class="fa fa-heart"></i></button>
									</div>
									
									<img src="./assets/img/product05.jpg" alt="">
								</div>
								<div class="product-body">
									<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
									
									<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
									<div class="product-btns">
										<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
										
										<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
									</div>
								</div>
							</div>
							<!-- /Product Single -->

							<!-- Product Single -->
							<div class="product product-single">
								<div class="product-thumb">
									
									<img src="./assets/img/product04.jpg" alt="">
								</div>
								<div class="product-body">
									<h3 class="product-price">₹32.50</h3>
								
									<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
									<div class="product-btns">
										<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
										
										<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
									</div>
								</div>
							</div>
							<!-- /Product Single -->

							<!-- Product Single -->
							<div class="product product-single">
								<div class="product-thumb">
									<div class="product-label">
										<span>New</span>
										<button class="main-btn quick-view"><i class="fa fa-heart"></i></button>
									</div>
									
									<img src="./assets/img/product03.jpg" alt="">
								</div>
								<div class="product-body">
									<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
									
									<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
									<div class="product-btns">
										<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
										
										<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
									</div>
								</div>
							</div>
							<!-- /Product Single -->

						</div>
					</div>
				</div>
				<!-- /Product Slick -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

	<!-- section -->
	<div class="section section-grey">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- banner -->
				<div class="col-md-8">
					<div class="banner banner-1">
						<img src="./assets/img/banner13.jpg" alt="">
						<div class="banner-caption text-center">
							<h1 class="primary-color">HOT DEAL<br><span class="white-color font-weak">Up to 50% OFF</span></h1>
							<button class="primary-btn">Shop Now</button>
						</div>
					</div>
				</div>
				<!-- /banner -->

				<!-- banner -->
				<div class="col-md-4 col-sm-6">
					<a class="banner banner-1" href="#">
						<img src="./assets/img/banner11.jpg" alt="">
						<div class="banner-caption text-center">
							<h2 class="white-color">NEW COLLECTION</h2>
						</div>
					</a>
				</div>
				<!-- /banner -->

				<!-- banner -->
				<div class="col-md-4 col-sm-6">
					<a class="banner banner-1" href="#">
						<img src="./assets/img/banner12.jpg" alt="">
						<div class="banner-caption text-center">
							<h2 class="white-color">NEW COLLECTION</h2>
						</div>
					</a>
				</div>
				<!-- /banner -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- section title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Latest Products</h2>
					</div>
				</div>
				<!-- section title -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							
							<img src="./assets/img/product01.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50</h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
								
							</div>
							
							<img src="./assets/img/product02.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
								
							</div>
							
							<img src="./assets/img/product03.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
							</div>
							
							<img src="./assets/img/product04.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50</h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->
			</div>
			<!-- /row -->

			<!-- row -->
			<div class="row">
			    	<!-- section title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Trends</h2>
					</div>
				</div>
				<!-- section title -->
				<!-- banner -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="banner banner-2">
						<img src="./assets/img/banner15.jpg" alt="">
						<div class="banner-caption">
							<h2 class="white-color">NEW<br>COLLECTION</h2>
							<button class="primary-btn">Shop Now</button>
						</div>
					</div>
				</div>
				<!-- /banner -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
								
							</div>
							
							<img src="./assets/img/product07.jpg" alt="">
							<button class="main-btn quick-view"><i class="fa fa-heart"></i></button>
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
								
							</div>
							
							<img src="./assets/img/product06.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
								
							</div>
							
							<img src="./assets/img/product05.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->
			</div>
			<!-- /row -->

			<!-- row -->
			<div class="row">
				<!-- section title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Recently Viewed</h2>
					</div>
				</div>
				<!-- section title -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							
							<img src="./assets/img/product04.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50</h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019 </h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
							</div>
							
							<img src="./assets/img/product03.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50</h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								
							</div>
							
							<img src="./assets/img/product02.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->

				<!-- Product Single -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
								
							</div>
							
							<img src="./assets/img/product01.jpg" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">₹32.50 <del class="product-old-price">₹45.00</del></h3>
							
							<h2 class="product-name1">Product Name </h2><h2 class="product-name"><a href="#">Shop Name</a> &nbsp;&nbsp 2 Aug 2019</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

	<!-- FOOTER -->
	<footer id="footer" class="section section-grey">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- footer widget -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="footer">
						<!-- footer logo -->
						<div class="footer-logo">
							<a class="logo" href="#">
		            <img src="./assets/img/logo.png" alt="">
		          </a>
						</div>
						<!-- /footer logo -->

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>

						<!-- footer social -->
						<ul class="footer-social">
							<li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter-square"></i></a></li>
							<li><a href="#"><i class="fab fa-instagram"></i></a></li>
							<li><a href="#"><i class="fab fa-google-plus-square"></i></a></li>
							<li><a href="#"><i class="fab fa-linkedin"></i></a></li>
						</ul>
						<!-- /footer social -->
					</div>
				</div>
				<!-- /footer widget -->

				<!-- footer widget -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="footer">
						<h3 class="footer-header">My Account</h3>
						<ul class="list-links">
							<li><a href="#">My Account</a></li>
							<li><a href="#">My Wishlist</a></li>
							<li><a href="#">Compare</a></li>
							<li><a href="#">Checkout</a></li>
							<li><a href="#">Login</a></li>
						</ul>
					</div>
				</div>
				<!-- /footer widget -->

				<div class="clearfix visible-sm visible-xs"></div>

				<!-- footer widget -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="footer">
						<h3 class="footer-header">Customer Service</h3>
						<ul class="list-links">
							<li><a href="#">About Us</a></li>
							<li><a href="#">Shiping & Return</a></li>
							<li><a href="#">Shiping Guide</a></li>
							<li><a href="#">FAQ</a></li>
						</ul>
					</div>
				</div>
				<!-- /footer widget -->

				<!-- footer subscribe -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="footer">
						<h3 class="footer-header">Stay Connected</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
						<form>
							<div class="form-group">
								<input class="input" placeholder="Enter Email Address">
							</div>
							<button class="primary-btn">Join Newslatter</button>
						</form>
					</div>
				</div>
				<!-- /footer subscribe -->
			</div>
			<!-- /row -->
			<hr>
			<!-- row -->
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<!-- footer copyright -->
					<div class="footer-copyright">
						
						Copyright &copy;2019 All rights reserved
						
					</div>
					<!-- /footer copyright -->
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</footer>
	<!-- /FOOTER -->

<div class="modal fade" id="modalQuickView" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-5">
            <!--Carousel Wrapper-->
            
             
              
                
                  <img class="d-block w-100 mh"
                    src="assets/img/login.jpg"
                    alt="First slide">
                </div>
                
              
             
          <div class="col-lg-7">
		 <div class="col-12 " style="text-align:right;">
		 <a class="pointesrcurs"  data-dismiss="modal" style="font-size:37px">&times;</a>

		 </div>
            <div class="card-body">
				
			<div class="res_form_inner">
        			<div class="form_title">
        				<h2>Login</h2>
        				<p>Get access to your Orders, Wishlist and Recommendations.</p>
        			</div>
        			<form class="reservation_form row" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
					<div class="form-group col-lg-12">
							<input type="email" class="form-control" id="email" name="email" placeholder="Email address">
						</div>
						<div class="form-group col-lg-12">
							<input type="password" class="form-control" id="pass" name="pass" placeholder="Password">
						</div>
						
						
						<div class="form-group col-lg-12">
							<button class="btn submit_btn" type="submit" value="submit">Log in</button>
                        </div>
                        <a href="#"  class="text-uppercase pointesrcurs" data-toggle="modal" data-target="#modalQuickView2"> New to ARBNB? Create an account</a>
                        <br>
                        OR
                        <br>
                        <p>Log in With<p>
                        <a type="button" class="btn-floating btn-fb"><i class="fab fa-facebook-f"></i></a>
                        <a type="button" class="btn-floating btn-gplus waves-effect waves-light">
            <i class="fab fa-google"></i>
          </a>
                    </form>
                    

        		</div>
              
            </div>
           
          </div>
          
        </div>
       
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modalQuickView2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-5">
            <!--Carousel Wrapper-->
            
             
              
                
                  <img class="d-block w-100 mh"
                    src="assets/img/login.jpg"
                    alt="First slide">
                </div>
                
              
             
          <div class="col-lg-7">
		 <div class="col-12 " style="text-align:right;">
		 <a class="pointesrcurs"  data-dismiss="modal" style="font-size:37px">&times;</a>

		 </div>
            <div class="card-body">
				
			<div class="res_form_inner">
        			<div class="form_title">
        				<h2>Register</h2>
        				<p>We do not share your personal details with anyone.</p>
        			</div>
        			<form class="reservation_form row" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                    <div class="form-group col-lg-12">
							<input type="text" class="form-control" id="email" name="email" placeholder="Full Name">
						</div>
                    <div class="form-group col-lg-12">
							<input type="email" class="form-control" id="email" name="email" placeholder="Email address">
                        </div>
                        <div class="form-group col-lg-12">
							<input type="email" class="form-control" id="email" name="email" placeholder="Mobile Number">
						</div>
						<div class="form-group col-lg-12">
							<input type="password" class="form-control" id="pass" name="pass" placeholder="Password">
                        </div>
                        <div class="form-group col-lg-12">
							<input type="password" class="form-control" id="pass" name="pass" placeholder="Confirm Password">
						</div>
						
						
						<div class="form-group col-lg-12">
							<button class="btn submit_btn" type="submit" value="submit">Register</button>
                        </div>
                                               <br>
                        <a href="#"  class="text-uppercase pointesrcurs" data-toggle="modal" data-target="#modalQuickView2"> Already in ARBNB? Go for Login</a>
                    </form>
                    

        		</div>
              
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Modal: modalCookie-->
<div class="modal fade top" id="modalCookie1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true" data-backdrop="true">
  <div class="modal-dialog modal-frame modal-top modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Body-->
      <div class="modal-body">
        <div class="row d-flex justify-content-center align-items-center">
        	<div class="col-md-6">
          <h2 class="product-name1">Select Your city</h2>

          
						</div>
						<div class="col-md-6">
							<select class="input search-categories">
								<option value="0">All India</option>
								<option value="1">Raipur</option>
                                <option value="2">Bhilai</option>
                                <option value="3">Durg</option>
								
							</select>
						</div>
        </div>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!--Modal: modalCookie-->


	<!-- jQuery Plugins -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/slick.min.js"></script>
	<script src="assets/js/nouislider.min.js"></script>
	
	<script src="assets/js/main.js"></script>
	
	<script>
		
		
			$('#modalCookie1').modal('show'); 
			
 // code here

		
	</script>
</body>

</html>
